package com.mobiwecraft.gymtracker.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mobiwecraft.gymtracker.R;
import com.mobiwecraft.gymtracker.utilities.Globall;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ProfileActivity extends AppCompatActivity {
    Context context;
    String[] gender = {"Male", "Female"};

    @InjectView(R.id.txtChangePassword) TextView change_password;
    @InjectView(R.id.txtDob) TextView txtDob;
    @InjectView(R.id.txtWeight) TextView txtWeight;
    @InjectView(R.id.txtHeight) TextView txtHeight;
    @InjectView(R.id.txtLogout) TextView txtLogout;
    @InjectView(R.id.txtGender) TextView txtGender;
    @InjectView(R.id.txtEmail) TextView txtEmail;
    RelativeLayout nameCard, emailCard, phoneCard,dateCard, regionCard,cityCard, resCard, passwordCard, logoutCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_profile);
        context=this;
        ButterKnife.inject(this);

        txtEmail.setText(Globall.userEmail);

        change_password.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(ProfileActivity.this, ChangePassword.class);
                startActivity(intent);
            }
        });


        txtGender.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(ProfileActivity.this)
                        .title("Select Gender")
                        .items(gender)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                txtGender.setText(gender[which]);
                                Globall.userGender=gender[which];
                                //updateProfile();

                                return true;
                            }
                        })
                        .show();

            }

        });
    }




    private void updateProfile(){
        /*
        final  String fname = _etFname.getText().toString().trim();
        final  String lname = _etLname.getText().toString().trim();
        final  String email = _emailText.getText().toString().trim();
        final  String password = _passwordText.getText().toString().trim();
        final String phone = _phone.getText().toString().trim();
        final String dob = _dob.getText().toString().trim();
        */
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Globall.UPDATE_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(ProfileActivity.this,response,Toast.LENGTH_LONG).show();
                        Log.e("Responsse", "Response from server"+response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProfileActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                        Log.e("Responsse", "Response from server"+error.toString());
                    }
                }){
            /*
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            */

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                params.put("action", "register");

                params.put("userEmail", "mashetin.91@gmail.com");//email);
                params.put("firstName","Neville");//fname);
                params.put("lastName","Masheti"); //lname);
                params.put("userPhone", "073432547"); //,phone);
                params.put("userDOB", "7-8-8190"); //dob);
                params.put("userPassword","888789" ); //password);
                /*
                JSONObject jsonobj= new JSONObject();
                try {
                    jsonobj.put("action", "register");
                    jsonobj.put("userEmail", "mashetin.91@gmail.com");//email);
                    jsonobj.put("firstName","Neville");//fname);
                    jsonobj.put("lastName","Masheti"); //lname);
                    jsonobj.put("userPhone", "073432547"); //,phone);
                    jsonobj.put("userDOB", "7-8-8190"); //dob);
                    jsonobj.put("userPassword","888789" ); //password);

                }catch (JSONException e) {
                    Log.e("JSON", "Json exception");
                }
                */



                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);

        requestQueue.add(stringRequest);
    }








}
