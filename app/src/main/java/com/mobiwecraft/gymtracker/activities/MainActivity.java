package com.mobiwecraft.gymtracker.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mobiwecraft.gymtracker.R;
import com.mobiwecraft.gymtracker.adapters.ExerciseAdapter;
import com.mobiwecraft.gymtracker.models.Equipment;
import com.mobiwecraft.gymtracker.models.Exercise;
import com.mobiwecraft.gymtracker.utilities.CustomStringRequest;
import com.mobiwecraft.gymtracker.utilities.Globall;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuParams;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements OnMenuItemClickListener{
    private ListView listView;
    private ExerciseAdapter myAppAdapter;
    private ArrayList<Exercise> exerciseArrayList;
    Context context;



    MenuParams menuParams;
    private FragmentManager fragmentManager;
    private ContextMenuDialogFragment mMenuDialogFragment;
    ProgressDialog pdialog;
    String auth;
    Map<String, String> params;;
    Map<String, String> headers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);


        context=this;
        initMenuFragment();
        pdialog=new ProgressDialog(this);

        listView= (ListView)findViewById(R.id.listView);
        //Log.e("Confirm", "Confirmed>>>>"+Globall.sel_servicez.get(0));



        if(Globall.exercises.size()>0)
        {
            myAppAdapter=new ExerciseAdapter(Globall.exercises, this, listView);
            listView.setAdapter(myAppAdapter);

        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

            }
        });

        //getData();
        get_equipment();



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SelectActivity.class);
                startActivity(intent);
            }
        });
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

            getMenuInflater().inflate(R.menu.menu_options, menu);
            return true;

    }




    private void initMenuFragment() {

        fragmentManager = getSupportFragmentManager();
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
        menuParams.setMenuObjects(Globall.getMenuObjects());
        menuParams.setClosableOutside(false);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        mMenuDialogFragment.setItemClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_menu) {
            mMenuDialogFragment.show(fragmentManager, "ContextMenuDialogFragment");

        }



        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onMenuItemClick(View clickedView, int position) {
        if(position==1)
        {



        }
        if(position==2)
        {

            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(intent);

        }

        else if(position==3)
        {

            Intent intent = new Intent(MainActivity.this, ReportActivity.class);
            startActivity(intent);

        }


        else
        {
            mMenuDialogFragment.dismiss();
        }
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        if(Globall.exerciseAdded)
        {
            myAppAdapter.notifyDataSetChanged();
            listView.invalidate();
            Toast.makeText(context, "Exercise added Successfully", Toast.LENGTH_LONG).show();
            Globall.exerciseAdded=false;
        }

    }

    private void getData(){
        //loader.show();
        Log.e("Start", "Started query web");


        StringRequest stringExercise = new StringRequest(Request.Method.POST, Globall.EXERCISES_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        response=response.replace("\t","");
                        Log.e("Responsse", "Response from serivies>>>"+response);


                        JSONObject jsonObj = null;
                        try {
                            jsonObj = new JSONObject(response);


                           if(true)
                           {


                            } else {
                                // animatedCircleLoadingView.stopFailure();
                                if(pdialog.isShowing())
                                {
                                    pdialog.dismiss();
                                }

                                new MaterialDialog.Builder(MainActivity.this)
                                        .title("Server Error ")
                                        .content("Loading data failed. Please try again later.")
                                        .positiveText("OK")
                                        .show();


//                                    Toast.makeText(LoginActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            Log.e("Catch ya service", "catch ya service"+e.toString());
                            if(pdialog.isShowing())
                            {
                                pdialog.dismiss();
                            }
                            new MaterialDialog.Builder(MainActivity.this)
                                    .title("Server Error ")
                                    .content("Loading data failed. Please try again later or contact us.")
                                    .positiveText("OK")
                                    .show();
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Responsse", "Response from server"+error.toString());
                        if(pdialog.isShowing())
                        {
                            pdialog.dismiss();
                        }
                        handleVolleyError(error);
                    }
                })
        {


            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                params.put("Authorisation ", Globall.auth_token);

                return params;
            }
        };

        StringRequest stringEquipment = new StringRequest(Request.Method.POST, Globall.EQUIPMENT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Responsse", "Response from aspirants>>>"+response);

                        JSONObject jsonObj = null;
                        //JSONObject jsonObj2 = null;
                        try {
                            jsonObj = new JSONObject(response);




                           if(true)
                           {

                            } else {
                               pdialog.dismiss();
                                new MaterialDialog.Builder(MainActivity.this)
                                        .title("Server Error ")
                                        .content("Loading data failed. Please try again later.")
                                        .positiveText("OK")
                                        .show();


//                                    Toast.makeText(LoginActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            Log.e("Catch ya service", "catch ya service"+e.toString());
                            pdialog.dismiss();
                            new MaterialDialog.Builder(MainActivity.this)
                                    .title("Server Error")
                                    .content("Error connecting to the server. Please try again or contact us.")
                                    .positiveText("OK")
                                    .show();
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Responsse", "Response from server"+error.toString());
                        pdialog.dismiss();
                        handleVolleyError(error);
                    }
                }){


            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                params.put("Authorisation ", Globall.auth_token);

                return params;
            }
        };

        stringExercise.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        pdialog.show();
        requestQueue.add(stringExercise);
        requestQueue.add(stringEquipment);

    }





    private void handleVolleyError(VolleyError error) {
        //if any error occurs in the network operations, show the TextView that contains the error message

        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            pdialog.dismiss();

            new MaterialDialog.Builder(MainActivity.this)
                    .title("Opps! Timeout Error")
                    .content("Problem with your internet connection")
                    .positiveText("OFFLINE ACCESS")
                    .negativeText("CANCEL")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {




                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .show();


        } else if (error instanceof AuthFailureError) {
            pdialog.dismiss();

            new MaterialDialog.Builder(MainActivity.this)
                    .title("Opps! Auth Failure Error")
                    .content("Server authentication failure")
                    .positiveText("OFFLINE ACCESS")
                    .negativeText("CANCEL")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {




                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .show();

        } else if (error instanceof ServerError) {
            pdialog.dismiss();

            new MaterialDialog.Builder(MainActivity.this)
                    .title("Opps! Server Error")
                    .content("Problem with the Web Server")
                    .positiveText("OFFLINE ACCESS")
                    .negativeText("CANCEL")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {



                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .show();


        } else if (error instanceof NetworkError) {
            pdialog.dismiss();
            new MaterialDialog.Builder(MainActivity.this)
                    .title("Opps! Network Error")
                    .content("Problem with your network")
                    .positiveText("OK")
                    .show();


        } else if (error instanceof ParseError) {
            pdialog.dismiss();
            new MaterialDialog.Builder(MainActivity.this)
                    .title("Opps! Parse Error")
                    .content("Cannot parse JSON data.Check your server configuration")
                    .positiveText("OK")
                    .show();

        }
    }


    public void get_equipment()
    {
        pdialog = new ProgressDialog(MainActivity.this);
        pdialog.setIndeterminate(true);
        pdialog.setMessage("Loading...");
        pdialog.show();
        headers =  new HashMap<>();

        auth="Bearer " + Globall.auth_token;
        headers.put("Authorization: ", this.auth);

        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);

        // Request a string response from the provided URL.
        CustomStringRequest stringEquipment = new CustomStringRequest(Request.Method.GET, Globall.EQUIPMENT_URL, headers,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        //Log.e("Response: ", "Response: "+response);

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            Globall.equipment.clear();

                            for(int k=0; k<jsonArray.length();k++)
                            {
                                JSONObject innerObjct = jsonArray.getJSONObject(k);
                                String id = String.valueOf(innerObjct.getInt("id"));
                                String equipname = innerObjct.getString("equipment_name");
                                String body_part = innerObjct.getString("body_part");


                                JSONObject imageObject = innerObjct.getJSONObject("equipment_image");

                                String img_url =imageObject.getString("url");

                                Equipment equipment = new Equipment(id, equipname,body_part , img_url );
                                Globall.equipment.add(equipment);
                                //Log.e("Object",equipment.getBodyPart());

                            }

                            pdialog.dismiss();



                        } catch (JSONException e) {
                            pdialog.dismiss();
                            Log.e("Catch", "catch ya Equipment:");
                            e.printStackTrace();
                        }





                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdialog.dismiss();
                        Log.e("Response: ", "error: "+error.toString());

                        //hello.setText(error.toString());
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }



                });

        CustomStringRequest stringExercises = new CustomStringRequest(Request.Method.GET, Globall.EXERCISES_URL, headers,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("Response: ", "Response: "+response);

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            Globall.exercises.clear();

                            for(int k=0; k<jsonArray.length();k++)
                            {

                                JSONObject innerObjct = jsonArray.getJSONObject(k);
                                String id = String.valueOf(innerObjct.getInt("id"));
                                String setz= String.valueOf(innerObjct.getInt("sets"));
                                String weightt= innerObjct.getString("weight_lifted");
                                String equipname= innerObjct.getString("equipment_name");
                                String body_part= innerObjct.getString("body_part");

                                JSONObject imageObject = innerObjct.getJSONObject("equipment_image").getJSONObject("equipment_image");
                                String img_url=imageObject.getString("url");
                                //Log.e("Img", imageObject.toString());



                                Exercise exercize = new Exercise(id,body_part,img_url, weightt,setz, equipname );
                                Globall.exercises.add(exercize);
                                //Log.e("Object",Globall.exercises.get(0).getEquipmentPicture());

                            }
                            myAppAdapter=new ExerciseAdapter(Globall.exercises, context, listView);
                            listView.setAdapter(myAppAdapter);

                            pdialog.dismiss();



                        } catch (JSONException e) {
                            pdialog.dismiss();
                            Log.e("Catch", "catch ya exercises");
                            e.printStackTrace();
                        }





                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdialog.dismiss();
                        Log.e("Response: ", "error: "+error.toString());

                        //hello.setText(error.toString());
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }



                });

        stringEquipment.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringExercises.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Add the request to the RequestQueue.
        queue.add(stringEquipment);
        queue.add(stringExercises);

    }


}





