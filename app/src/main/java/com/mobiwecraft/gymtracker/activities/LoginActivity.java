package com.mobiwecraft.gymtracker.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mobiwecraft.gymtracker.R;
import com.mobiwecraft.gymtracker.utilities.Globall;


import javax.net.ssl.HttpsURLConnection;

public class LoginActivity extends AppCompatActivity{
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private final String  LOGIN_KEY = "logInKey";
    Context context;
    private FingerprintManager fingerprintManager;

    @InjectView(R.id.input_email) EditText _emailText;
    @InjectView(R.id.input_password) EditText _passwordText;
    @InjectView(R.id.btn_login) TextView _loginButton;
    @InjectView(R.id.link_signup) TextView _signupLink;
    @InjectView(R.id.link_fpassword) TextView _fpassword;
    boolean error,loggedIn;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;




        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);




        ButterKnife.inject(this);



        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {



                if (Globall.isConnectingToInternet(LoginActivity.this)) {

                    loginUser();
                    //Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    //startActivity(intent);


                } else {

                    new MaterialDialog.Builder(LoginActivity.this)
                            .title("Connection Error")
                            .content("Not connected to the Internet. Enable data or check bundles")
                            .positiveText("OK")
                            .show();
                }
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity

                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });
        _fpassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), ForgotPassword.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);

            }
        }
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(LoginActivity.this)
                .title("Exit App")
                .content("Do you want to exit the application? ")
                .positiveText("OK")
                .negativeText("CANCEL")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();

                    }
                })
                .show();
    }

    public void onLoginSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 ) {
            _passwordText.setError("alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    private void handleVolleyError(VolleyError error) {
        //if any error occurs in the network operations, show the TextView that contains the error message

        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            new MaterialDialog.Builder(LoginActivity.this)
                    .title("Opps! Timeout Error")
                    .content("Problem with your internet connection")
                    .positiveText("OK")
                    .show();

        } else if (error instanceof AuthFailureError) {
            new MaterialDialog.Builder(LoginActivity.this)
                    .title("Opps! Auth Failure Error")
                    .content("Server authentication failure")
                    .positiveText("OK")
                    .show();

        } else if (error instanceof ServerError) {
            new MaterialDialog.Builder(LoginActivity.this)
                    .title("Opps! Server Error")
                    .content("Problem with the teller Web Server")
                    .positiveText("OK")
                    .show();

        } else if (error instanceof NetworkError) {
            new MaterialDialog.Builder(LoginActivity.this)
                    .title("Opps! Network Error")
                    .content("Problem with your network")
                    .positiveText("OK")
                    .show();

        } else if (error instanceof ParseError) {
            new MaterialDialog.Builder(LoginActivity.this)
                    .title("Opps! Parse Error")
                    .content("Cannot parse JSON data.Check your server configuration")
                    .positiveText("OK")
                    .show();

        }
    }



    private void loginUser(){


        final  String email = _emailText.getText().toString().trim();
        final  String password = _passwordText.getText().toString().trim();



        Log.d(TAG, "Login");

        //if (!validate()) {
           // onLoginFailed();
           // return;
        //}




        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Globall.LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", "Response from server"+response);

                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                        JSONObject jsonObj = null;
                        Globall.auth_token="";
                        

                        try {
                            jsonObj = new JSONObject(response);
                            //Log.e("Response", "Response Object"+jsonObj);

                            //dataArray = jsonObj.getJSONArray("data");
                            Globall.auth_token=jsonObj.getString("auth_token");
                           if(!Globall.auth_token.trim().matches(""))
                           {
                               Globall.userEmail=email;
                               Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                               startActivity(intent);


                           } else {
                                new MaterialDialog.Builder(LoginActivity.this)
                                        .title("Login Failed")
                                        .content("Invalid Email or Password.")
                                        .positiveText("OK")
                                        .show();


//                                    Toast.makeText(LoginActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("sgghdg", e.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Responsse", "Response from server"+error.toString());
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                        handleVolleyError(error);
                    }
                }){

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                params.put("email", email);
                params.put("password",password);

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Adding the string request to the queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);


        requestQueue.add(stringRequest);


    }

    boolean hasPermission(String permission) {
        return context.checkCallingOrSelfPermission(permission)
                == PackageManager.PERMISSION_GRANTED;
    }


}