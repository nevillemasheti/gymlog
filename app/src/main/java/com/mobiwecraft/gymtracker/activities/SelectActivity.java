package com.mobiwecraft.gymtracker.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mobiwecraft.gymtracker.R;
import com.mobiwecraft.gymtracker.adapters.ExerciseAdapter;
import com.mobiwecraft.gymtracker.adapters.SelectAdapter;
import com.mobiwecraft.gymtracker.models.Equipment;
import com.mobiwecraft.gymtracker.models.Exercise;
import com.mobiwecraft.gymtracker.utilities.CustomStringRequest;
import com.mobiwecraft.gymtracker.utilities.Globall;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuParams;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL;

public class SelectActivity extends AppCompatActivity{
    private ListView listView;
    private SelectAdapter myAppAdapter;
    private ArrayList<Equipment> equipmentArrayList;
    Context context;

    ProgressDialog pdialog;

    String auth;
    Map<String, String> params;;
    Map<String, String> headers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_select);


        context=this;

        pdialog=new ProgressDialog(this);

        listView= (ListView)findViewById(R.id.listView);
        //Log.e("Confirm", "Confirmed>>>>"+Globall.sel_servicez.get(0));


        if(Globall.equipment.size()>0)
        {
            myAppAdapter=new SelectAdapter(Globall.equipment, this, listView);
            listView.setAdapter(myAppAdapter);
        }
        else
        {
            Log.e("error", "Nothing to show");
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {

                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
                View mView = layoutInflaterAndroid.inflate(R.layout.input_dialog, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
                alertDialogBuilderUserInput.setView(mView);

                final EditText etWeight = (EditText) mView.findViewById(R.id.etWeight);
                final EditText etSets = (EditText) mView.findViewById(R.id.etSets);
                final TextView selExercise = (TextView) mView.findViewById(R.id.txtSelExercise);
                selExercise.setText(Globall.equipment.get(position).getBodyPart() + " - "+ Globall.equipment.get(position).getEquipmentName());

                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                if(etWeight.getText().toString().trim().matches("") || etSets.getText().toString().trim().matches("") )
                                {
                                    Toast.makeText(context,"Missing data. Please enter both weight and number of sets", Toast.LENGTH_LONG).show();
                                }
                                else
                                {

                                    postExerciseToAPI(Globall.equipment.get(position).getId(), etSets.getText().toString().trim(), etWeight.getText().toString().trim());
                                }


                            }
                        })

                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();

            }
        });





    }



    public void post_exercise(String equip_id, String sets, String weight_lifted)
    {
        try
        {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("equipment_id",   equip_id);
            jsonBody.put("sets",  sets);
            jsonBody.put("weight_lifted", weight_lifted);
            final String requestBody = jsonBody.toString();

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        pdialog = new ProgressDialog(SelectActivity.this);
        pdialog.setIndeterminate(true);
        pdialog.setMessage("Posting...");
        pdialog.show();
        headers =  new HashMap<>();

        auth="Bearer " + Globall.auth_token;
        headers.put("Authorization: ", this.auth);

        RequestQueue queue = Volley.newRequestQueue(SelectActivity.this);

        // Request a string response from the provided URL.
        CustomStringRequest stringPostExercise = new CustomStringRequest(Request.Method.POST, Globall.EXERCISES_URL, headers,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("Response: ", "Response: "+response);

                        try {
                            JSONArray jsonArray = new JSONArray(response);


                            pdialog.dismiss();
                            finish();
                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);



                        } catch (JSONException e) {
                            pdialog.dismiss();
                            Log.e("Catch", "catch:");
                            e.printStackTrace();
                        }





                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdialog.dismiss();
                        Log.e("Response: ", "error: "+error.toString());

                        //hello.setText(error.toString());
                        Toast.makeText(SelectActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }



                });


        stringPostExercise.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Add the request to the RequestQueue.
        queue.add(stringPostExercise);

    }


    public void postExerciseToAPI(String equip_id, String sets, String weight_lifted){

        try {
            pdialog = new ProgressDialog(SelectActivity.this);
            pdialog.setIndeterminate(true);
            pdialog.setMessage("Posting...");
            pdialog.show();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("equipment_id",   equip_id);
            jsonBody.put("sets",  sets);
            jsonBody.put("weight_lifted", weight_lifted);
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Globall.EXERCISES_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(pdialog.isShowing())
                    {
                        pdialog.dismiss();
                    }
                    finish();
                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SelectActivity.this, "Post successful" + error.toString(), Toast.LENGTH_LONG).show();
                    if(pdialog.isShowing())
                    {
                        pdialog.dismiss();
                    }

                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        //VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        Toast.makeText(SelectActivity.this, "Unsupported Encoding while trying to get the bytes of %s using %s", Toast.LENGTH_LONG).show();
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    String auth = "Bearer " + Globall.auth_token;
                    params.put("Authorization: ", auth);
                    return params;
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
            if(pdialog.isShowing())
            {
                pdialog.dismiss();
            }
        }





    }


}


