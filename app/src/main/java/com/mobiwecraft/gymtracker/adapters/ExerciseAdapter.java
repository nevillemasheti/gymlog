package com.mobiwecraft.gymtracker.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.mobiwecraft.gymtracker.R;
import com.mobiwecraft.gymtracker.models.Exercise;
import com.mobiwecraft.gymtracker.utilities.Globall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.mobiwecraft.gymtracker.R;
import com.squareup.picasso.Picasso;


public class ExerciseAdapter extends BaseAdapter {

    public class ViewHolder {


        ImageView imgEquipment, imgDelete, imgEdit, imgShowMenu;

        TextView txtWeight, txtSets, txtBodyPart;
        LinearLayout buttonSec;


    }

    public List<Exercise> parkingList;

    public Context context;
    ArrayList<Exercise> arraylist;
    ListView listView;
    ExerciseAdapter exerciseAdapter;
    ProgressDialog pdialog;


    public ExerciseAdapter(List<Exercise> apps, Context context, ListView listView) {
        this.parkingList = apps;
        this.context = context;
        this.listView=listView;
        arraylist = new ArrayList<Exercise>();
        arraylist.addAll(parkingList);
        exerciseAdapter=this;
        pdialog=new ProgressDialog(context);


    }

    @Override
    public int getCount() {
        return parkingList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        final ViewHolder viewHolder;

        if (rowView == null) {
            LayoutInflater inflater=LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.exercise_card, null);

            // configure view holder
            viewHolder = new ViewHolder();

            viewHolder.txtWeight = (TextView)rowView.findViewById(R.id.txtWeight);
            viewHolder.txtSets = (TextView)rowView.findViewById(R.id.txtSets);
            viewHolder.txtBodyPart = (TextView)rowView.findViewById(R.id.txtBodyPart);
            viewHolder.imgDelete = (ImageView)rowView.findViewById(R.id.imgDelete);
            viewHolder.imgEdit = (ImageView)rowView.findViewById(R.id.imgEdit);
            viewHolder.imgEquipment= (ImageView)rowView.findViewById(R.id.imgEquipment);
            viewHolder.imgShowMenu= (ImageView)rowView.findViewById(R.id.showMenu);
            viewHolder.buttonSec= (LinearLayout) rowView.findViewById(R.id.buttonSec);





            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.imgShowMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewHolder.buttonSec.getVisibility()==View.GONE)
                {
                    viewHolder.buttonSec.setVisibility(View.VISIBLE);
                    viewHolder.imgShowMenu.setImageResource(R.mipmap.ic_close);
                }
                else
                {
                    viewHolder.buttonSec.setVisibility(View.GONE);
                    viewHolder.imgShowMenu.setImageResource(R.mipmap.showmenu);

                }
            }
        });


        viewHolder.txtBodyPart.setText(parkingList.get(position).getBodyPart());
        viewHolder.txtWeight.setText(parkingList.get(position).getWeight() +" kg");
        viewHolder.txtSets.setText(parkingList.get(position).getSets()+ " sets");
        //Log.e("Image", "Image URL"+parkingList.get(position).getEquipmentPicture());

        Picasso.with(context)
                .load(parkingList.get(position).getEquipmentPicture())
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.noimage)      // optional
                .into(viewHolder.imgEquipment);


        viewHolder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });





        return rowView;


    }

    public void delete_exercise(Exercise exercise, int  pos)
    {
        final int poz=pos;


        pdialog.setTitle("Deleting.....");
        pdialog.setIndeterminate(true);
        pdialog.show();

                pdialog.dismiss();
                Globall.exercises.remove(poz);
                exerciseAdapter.notifyDataSetChanged();
                listView.invalidate();




    }




}
