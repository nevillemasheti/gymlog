package com.mobiwecraft.gymtracker.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.mobiwecraft.gymtracker.R;
import com.mobiwecraft.gymtracker.models.Equipment;
import com.mobiwecraft.gymtracker.models.Exercise;
import com.mobiwecraft.gymtracker.utilities.Globall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.mobiwecraft.gymtracker.R;
import com.squareup.picasso.Picasso;


public class SelectAdapter extends BaseAdapter {

    public class ViewHolder {


        ImageView imgEquipment;

        TextView txtName, txtBodyPart;



    }

    public List<Equipment> parkingList;

    public Context context;
    ArrayList<Equipment> arraylist;
    ListView listView;
    SelectAdapter selectAdapter;
    ProgressDialog pdialog;


    public SelectAdapter(List<Equipment> apps, Context context, ListView listView) {
        this.parkingList = apps;
        this.context = context;
        this.listView=listView;
        arraylist = new ArrayList<Equipment>();
        arraylist.addAll(parkingList);
        selectAdapter=this;
        pdialog=new ProgressDialog(context);


    }

    @Override
    public int getCount() {
        return parkingList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        final ViewHolder viewHolder;

        if (rowView == null) {
            LayoutInflater inflater=LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.equipment_card, null);

            // configure view holder
            viewHolder = new ViewHolder();

            viewHolder.txtName = (TextView)rowView.findViewById(R.id.txtName);
            viewHolder.txtBodyPart = (TextView)rowView.findViewById(R.id.txtBodyPart);

            viewHolder.imgEquipment= (ImageView)rowView.findViewById(R.id.imgEquipment);









            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtName.setText(parkingList.get(position).getEquipmentName());
        viewHolder.txtBodyPart.setText(parkingList.get(position).getBodyPart());

        Log.e("Image", "Image Equipment URL"+parkingList.get(position).getImageUrl());

        Picasso.with(context)
                .load(parkingList.get(position).getImageUrl())
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.noimage)      // optional
                .into(viewHolder.imgEquipment);






        return rowView;


    }




}
