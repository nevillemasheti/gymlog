package com.mobiwecraft.gymtracker.models;


public class Equipment {

    String id, equipmentName, bodyPart, imageUrl;

    public Equipment(String id, String equipmentName, String bodyPart, String imageUrl) {
        this.id = id;
        this.equipmentName = equipmentName;
        this.bodyPart = bodyPart;
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getBodyPart() {
        return bodyPart;
    }

    public void setBodyPart(String bodyPart) {
        this.bodyPart = bodyPart;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
