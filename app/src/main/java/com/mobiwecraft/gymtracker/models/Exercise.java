package com.mobiwecraft.gymtracker.models;

/**
 * Created by Neville Masheti on 5/9/2017.
 */

public class Exercise {

    String id, bodyPart, equipmentPicture, weight, sets, equipname;


    public Exercise(String id, String bodyPart, String equipmentPicture, String weight, String sets, String equipname) {
        this.id = id;
        this.bodyPart = bodyPart;
        this.equipmentPicture = equipmentPicture;
        this.weight = weight;
        this.sets = sets;
        this.equipname=equipname;
    }

    public String getEquipname() {
        return equipname;
    }

    public void setEquipname(String equipname) {
        this.equipname = equipname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBodyPart() {
        return bodyPart;
    }

    public void setBodyPart(String bodyPart) {
        this.bodyPart = bodyPart;
    }

    public String getEquipmentPicture() {
        return equipmentPicture;
    }

    public void setEquipmentPicture(String equipmentPicture) {
        this.equipmentPicture = equipmentPicture;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getSets() {
        return sets;
    }

    public void setSets(String sets) {
        this.sets = sets;
    }
}
