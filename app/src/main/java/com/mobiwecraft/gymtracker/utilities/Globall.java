package com.mobiwecraft.gymtracker.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.mobiwecraft.gymtracker.R;
import com.mobiwecraft.gymtracker.models.Equipment;
import com.mobiwecraft.gymtracker.models.Exercise;
import com.yalantis.contextmenu.lib.MenuObject;

import java.util.ArrayList;
import java.util.List;


public class Globall {

    public static String EXERCISES_URL="http://mestergymlog.herokuapp.com/api/v1/user_exercises.json";

    public static String EQUIPMENT_URL="http://mestergymlog.herokuapp.com/api/v1/equipments.json";

    public static String LOGIN_URL="http://mestergymlog.herokuapp.com/api/v1/authenticate";
    public static String UPDATE_PROFILE="http://mestergymlog.herokuapp.com/api/v1/authenticate";

    public static String auth_token="";

    public static String userEmail="";


    public static String userGender=" ";
    public static Boolean exerciseAdded= false;

    public static ArrayList<Exercise> exercises= new ArrayList<Exercise>();
    public static ArrayList<Equipment> equipment= new ArrayList<Equipment>();

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }

        }
        return false;
    }



    public static List<MenuObject> getMenuObjects() {

        List<MenuObject> menuObjects = new ArrayList<>();

        MenuObject close = new MenuObject();
        close.setResource(R.mipmap.ic_close);

        MenuObject exercise = new MenuObject("Exercise");
        exercise.setResource(R.mipmap.ic_edit);

        MenuObject profile = new MenuObject("Profile");
        profile.setResource(R.mipmap.ic_edit);

        MenuObject report = new MenuObject("Fitness Report");
        report.setResource(R.mipmap.ic_edit);



        MenuObject cancel = new MenuObject("Close");
        cancel.setResource(R.mipmap.ic_close);

        menuObjects.add(close);
        menuObjects.add(exercise);
        menuObjects.add(profile);
        menuObjects.add(report);

        menuObjects.add(cancel);
        return menuObjects;
    }
}
